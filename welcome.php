<?php
session_start();

if(!isset($_SESSION["visit"])){
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	$value = $ipaddress;
	$_SESSION["visit"] = $value;
}

include("connect.php");
$val = $_SESSION["visit"];
$time = time();
if(isset($_SESSION["user_id"])){
	$user = $_SESSION["user_id"];
}
else{
	$user = 0;
}

mysqli_query($conn, "INSERT INTO `visits` (`user_id`, `session_key`, `page`, `time`) VALUES ('$user', '$val', 'Homepage', '$time')");
?>
<!DOCTYPE html>
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135509510-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-135509510-1');
</script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c9754c0c37db86fcfcf8c16/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
	<title>TradingCompared | Optimising every beginner trader for the market</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="move_to_mobile.js"></script>
<meta name="Description" content="Welcome to TradingCompared. Compare the different brokers, educators and signals available. Trading212, Plus500 and many more.">
<meta name="Keywords" content="compare trader, compare brokers, compare broker, broker, trading, compare trading, optimise markets, trading compared">
	<title></title>
</head>
<body>

	<div id="backdrop"></div>
	<div id="menu_color"></div>
	<div id="desktop_navigation_bar">

		<div id="desktop_navigation_bar_left" >
		<div id="desktop_navigation_bar_logo">
			<img src="arrows.png" id="desktop_navigation_bar_image">
		</div>
		<div id="desktop_navigation_bar_name" onclick='location.href="index.php"'>
			Trading Compared
		</div>
		</div>

	<a href='brokers.php'><div class="menu_action">BROKERS</div></a>
	<a href='education.php'><div class="menu_action">EDUCATION</div></a>
	<a href='signals.php'><div class="menu_action">SIGNALS</div></a>
	<a href='about.php'><div class="menu_action" style='width: 200px'>REGISTER INTEREST</div></a>

	<?php

	if(isset($_SESSION["user_id"])){
		include("connect.php");
		$user_id = $_SESSION["user_id"];
	 	$sql = "SELECT * FROM `users` WHERE `user_id`='$user_id'";
	 	$query = mysqli_query($conn, $sql);
		while($row = mysqli_fetch_assoc($query)){ $name = $row["first_name"]; }
		?>
		<a href='user.php'><div class="menu_action" style="width: 250px">WELCOME <?php echo strtoupper($name); ?></div></a>
		<?php
	}
	else{
		?>
	<div style="width: 320px;float: right;">
	<a href='login.php'><div class="menu_action">LOGIN</div></a>
	<a href='register.php'><div class="menu_action">
		<div id="register">REGISTER</div>
	</div></a>
	</div>
	</div>

		<?php
	}
	?>
</div>
<!-- End of navigation -->

<div id="home_left" style="position: absolute;top: 130px; left: 150px; color: white">
	<br><br>
<span style="font-family:'bold'; font-size: 80px">TRADING<br>COMPARED</span><br>
<span style="font-size: 24px;"><i>Optimising every beginner trader for the<br>markets</i></span>
</div>

<div id="home_right" style="position: absolute;top: 130px; left: 750px; color: white">
	<br><br>
<div style="width: 500px;background-color: white;box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
	<div style="width: 350px; padding: 50px">
		<span style="font-family:'bold'; font-size: 26px; color: #1c1c1c">Free Consultation</span>
		<br><br>
		<input style="width: calc(400px - 40px);padding: 20px;margin-bottom: 5px" placeholder="Name">
		<input style="width: calc(400px - 40px);padding: 20px;margin-bottom: 5px" placeholder="Email">
		<input style="width: calc(400px - 40px);padding: 20px;margin-bottom: 5px" placeholder="Phone">
		<textarea style="width: calc(400px - 40px);padding: 20px;margin-bottom: 5px;" placeholder="Phone"></textarea>
		<br><br		<div style=>
"text-align: center">
		<input type="submit" style="width: 200px; background-color: #4CCDF7; height: 50px;border-radius: 12px; line-height: 50px; font-size: 14px; text-align: center; margin: auto; color: white; font-family: 'bold'; box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23); border: 0px">
	</div>


</div>
</div>
</div>
<br><br><br><br><br><br>
<div style="width: 100vw; height: 300px">


	<div style="width: 800px; margin-left: 150px">

		<div style="float: left; width: 150px">
			<img src="employee.png">
		</div>
		<div style="float: left; width: 600px">
			<span style="font-family: 'bold'; font-size: 30px">Brokers</span>
			<div style="width: 90px; height: 6px; background-color: #4bcdf8"></div>
			<br>
			<span style="font-family: 'test'; font-size: 15px">Brokers are the in-between men, they allow you to buy and sell in the markets, they find a seller for what you want to buy. Every trader needs one in order to trade.<br><br>

We compare the best brokers aimed at beginners, providing full information of what they offer to you as an individual.
<br><br>
Our promoted brokers offer trading of Forex, Stocks, Cryptocurrency and so much more. We have found the best at the cheapest prices for you.</span>

		</div>

	</div>


</div>
<br><br><br>
<div style="background-color: #f2f6fa; width: 100vw; height: 400px">
<br><br>

	<div style="width: 800px; margin-left: 150px">

		<div style="float: left; width: 150px">
			<img src="graduation-hat.png">
		</div>
		<div style="float: left; width: 600px">
			<span style="font-family: 'bold'; font-size: 30px">Education</span>
			<div style="width: 90px; height: 6px; background-color: #4bcdf8"></div>
			<br>
			<span style="font-family: 'test'; font-size: 15px">Education will teach you how the markets work and how you can become the profitable trader you've always wanted.<br><br>We have scoured the trading world to find the best beginner based education, with varying price ranges and payment models.<br><br>Courses, videos, 1 to 1 and subscription based training is all compared on this section. We are positive the education you need is here and waiting for you.</span>

		</div>

	</div>


</div>
<br>
<div style=" width: 100vw; height: 400px">
<br><br>

	<div style="width: 800px; margin-left: 150px">

		<div style="float: left; width: 150px">
			<img src="signal.png">
		</div>
		<div style="float: left; width: 600px">
			<span style="font-family: 'bold'; font-size: 30px">Signals</span>
			<div style="width: 90px; height: 6px; background-color: #4bcdf8"></div>
			<br>
			<span style="font-family: 'test'; font-size: 15px">Signals are trades that you follow from professional traders. They tell you what to buy and sell, when and how.<br><br>The ones we have gathered will help you to earn money from trading with no knowledge, you can do this whilst using the education so you can soon trade on your own behalf.</span>

		</div>

	</div>


</div>

<div style="width: 100vw; height: auto; background-color: #05222b;">
	<div style="width: calc(100vw - 300px); margin: auto;padding-top: 10px">

		<div id="desktop_navigation_bar_left" style=" margin-left: 0px; line-height: 40px; height: 40px" >
		<div id="desktop_navigation_bar_logo" >
			<img src="arrows.png" id="desktop_navigation_bar_image" style="margin-top: 5px">
		</div>
		<div id="desktop_navigation_bar_name" style="color: grey; line-height: 40px">
			Trading Compared
		</div>
		</div>
	<div style="float: right">
	<a href='brokers.php'><div class="menu_action" style="line-height: 40px; height: 40px">BROKERS</div></a>
	<a href='education.php'><div class="menu_action" style="line-height: 40px; height: 40px">EDUCATION</div></a>
	<a href='signals.php'><div class="menu_action" style="line-height: 40px; height: 40px">SIGNALS</div></a>
	<a href='about.php'><div class="menu_action" style="line-height: 40px; height: 40px">ABOUT US</div></a>
</div>
	<div style="clear:both"></div>
	<Br>

	<div style="font-size: 11px; color: #839eb6; text-align: center"><br>
		Disclaimer: By trading with securities you are taking a high degree of risk. You can lose all of your invested money. You should start trading only if you are aware of this risk. tradingcompared.co.uk is not providing any investment advice, we only help you find the best broker suitable for your needs. tradingcompared is free for everyone, but earns commission from some of the brokers. We get a commission, with no additional cost for you. Please use our link to open your account and we can further provide broker reviews for free.
		<br><br><Br>
		Copyright 2019 TradingCompared All Rights Reserved	
		<br><br>	

	</div>

	</div>

</div>



</body>
</html>
<link rel="stylesheet" type="text/css" href="index.css">