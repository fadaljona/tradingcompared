<html>
	<head>
		<link rel="stylesheet" type="text/css" href="discover.css">
		<title>TradingCompared | Optimising every beginner trader for the market</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="move_to_mobile.js"></script>
		<meta name="Description" content="Welcome to TradingCompared. Compare the different brokers, educators and signals available. Trading212, Plus500 and many more.">
		<meta name="Keywords" content="compare trader, compare brokers, compare broker, broker, trading, compare trading, optimise markets, trading compared">
				
		</head>
		<body style="background-color: #7BB5B8;">
			<div id="desktop_navigation_bar">
				<div id="desktop_navigation_bar_left" >
				<div id="desktop_navigation_bar_logo">
					<!-- <img src="arrows.png" id="desktop_navigation_bar_image"> -->
				</div>
				<div id="desktop_navigation_bar_name">
					Trading Compared
				</div>
				</div>

			<?php

			session_start(); if(isset($_SESSION["user_id"])){
				include("connect.php");
				$user_id = $_SESSION["user_id"];
			 	$sql = "SELECT * FROM `users` WHERE `user_id`='$user_id'";
			 	$query = mysqli_query($conn, $sql);
				while($row = mysqli_fetch_assoc($query)){ $name = $row["first_name"]; }
				?>
				<a href='user.php'><div class="menu_action" style="width: 250px">WELCOME <?php echo strtoupper($name); ?></div></a>
				<?php
			}
			else{
				?>
			<div style="width: 300px;float: right; height: 130px;">
			<a href='register.php'>
				<div id="register" style="margin:auto; margin-top: 40px">Login</div>
			</div></a>

				<?php
			}
			?>
			<a href='about.php'><div class="menu_action">About Us</div></a>
			<a href='new_post.php'><div class="menu_action">Organise</div></a>
			<a href='dashboard.php'><div class="menu_action">Dashboard</div></a>
			
			

			<div style="clear: both"></div>
		</div>
		<Br>
		<h1 style="font-weight: lighter; font-size: 55px; text-align: center">Discover</h1>
		<h3 style="font-weight: lighter; text-align: center">An online community of passionate people</h3>
		<div style="text-align: center"><br><br>
		<form action="search.php" method="get">
		<input type="text" placeholder="Find A Protest ..." name="query" style="width: 550px;padding: 20px;border: 0px; border-radius: 5px">
		</form>
		</div>
</div>
<br><br><br>
<div style="width: 960px; margin: auto">
<?php

include("connect.php");

$sql = "SELECT * FROM `protests`";
$user = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($user)){

	$selected = $row["user_id"];

	$selected_user = mysqli_query($conn, "SELECT * FROM users WHERE `user_id`='$selected'");

	while ($rrow = mysqli_fetch_assoc($selected_user)) {

		$photo = $rrow["photo"];
		$name = $rrow["first_name"] . " " . $rrow["last_name"];

	}

	// echo $row["title"];
	?>
	<div class="result_card" onclick="location.href='protest.php?id=<?php echo $row["protest_id"];?>'">
	
		<div class="result_card_creator">


			<div class="result_card_creator_right">
				<a href='profile.php?id=<?php echo $row["user_id"];?>'><?php echo $name; ?></a><br>
				<?php echo $row["creation_timestamp"] ;?>

			</div>
			<div style="clear: both"></div>
		</div>
		<div class="result_card_title"><?php echo $row["title"]; ?></div>
		<!-- <div class="result_card_location"><?php echo $row["location"]; ?></div> -->
		<div class="result_card_description"><?php echo $row["description"]; ?></div>



	</div>
	<?php
}

?>
<style>

.result_card_creator{
	width: 280px;
	/*background-color: red;*/
	margin-left: 10px;
	margin-top: 10px;	
}
.result_card_creator_left{
	float : left;
	width: 60px;
	height: 60px;
	/*background-color: yellow*/
}

.result_card_creator_left img{
	width: 50px;
	margin: 5px;
	border-radius: 50%;
	height: 50px;
}

.result_card_creator_right{
	/*background-color: green;*/
	float: left;
	line-height: 30px;
	width:220px;
}

.result_card_title{
	width: 270px;
	/*background-color: red;*/
	margin-left: 15px;
	margin-top: 5px;
	color: black;
	font-size:16px;
}
.result_card_location{
	width: 270px;
	/*background-color: red;*/
	margin-left: 15px;
	font-size: 12px;
	color: #1c1c1c;
}

.result_card_description{
	margin-top: 5px;
	width: 270px;
	/*background-color: red;*/
	margin-left: 15px;
	font-size: 12px;
	color: #1c1c1c;
}


.result_card{
	background-color: white;
	width: 300px;
	height: 200px;
	float: left;
	margin: 10px;
	box-shadow: 0px 0px 5px lightgrey; border-radius: 8px;
}
</style>


<!-- black = #2F2E2E
blue = #7BB5B8
button black = #151515 -->