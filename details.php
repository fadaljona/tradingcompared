<?php
session_start();

if(!isset($_SESSION["visit"])){
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	$value = $ipaddress;
	$_SESSION["visit"] = $value;
}

include("connect.php");
$val = $_SESSION["visit"];
$time = time();
if(isset($_SESSION["user_id"])){
	$user = $_SESSION["user_id"];
}
else{
	$user = 0;
}

mysqli_query($conn, "INSERT INTO `visits` (`user_id`, `session_key`, `page`, `time`) VALUES ('$user', '$val', 'Detail ', '$time')");
?>
<!DOCTYPE html>
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135509510-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-135509510-1');
</script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c9754c0c37db86fcfcf8c16/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
	<title>TradingCompared | Optimising every beginner trader for the market</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="move_to_mobile.js"></script>
<meta name="Description" content="Welcome to TradingCompared. Compare the different brokers, educators and signals available. Trading212, Plus500 and many more.">
<meta name="Keywords" content="compare trader, compare brokers, compare broker, broker, trading, compare trading, optimise markets, trading compared">
	<title></title>
</head>
<body>

	<div id="desktop_navigation_bar" style="background-color: #05222b;">

		<div id="desktop_navigation_bar_left" >
		<div id="desktop_navigation_bar_logo">
			<img src="arrows.png" id="desktop_navigation_bar_image">
		</div>
		<div id="desktop_navigation_bar_name" onclick='location.href="index.php"'>
			Trading Compared
		</div>
		</div>

	<a href='brokers.php'><div class="menu_action">BROKERS</div></a>
	<a href='education.php'><div class="menu_action">EDUCATION</div></a>
	<a href='signals.php'><div class="menu_action">SIGNALS</div></a>
	<a href='about.php'><div class="menu_action" style='width: 200px'>REGISTER INTEREST</div></a>

	<?php

	if(isset($_SESSION["user_id"])){
		include("connect.php");
		$user_id = $_SESSION["user_id"];
	 	$sql = "SELECT * FROM `users` WHERE `user_id`='$user_id'";
	 	$query = mysqli_query($conn, $sql);
		while($row = mysqli_fetch_assoc($query)){ $name = $row["first_name"]; }
		?>
		<a href='user.php'><div class="menu_action">WELCOME <?php echo strtoupper($name); ?></div></a>
		<?php
	}
	else{
		?>
	<div style="width: 320px;float: right;">
	<a href='login.php'><div class="menu_action">LOGIN</div></a>
	<a href='register.php'><div class="menu_action">
		<div id="register">REGISTER</div>
	</div></a>
	</div>
	</div>

		<?php
	}
	?>
</div>
<!-- End of navigation -->]

			<?php

include("connect.php");
$id = $_GET["id"];
$type = $_GET["type"];
$sql = "SELECT * FROM `$type` WHERE `broker_id` ='$id'";
$user = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($user)){

	$namee = $row["title"];
	$a = $row["a"];
	$b = $row["b"];
	$c = $row["c"];
	$d = $row["d"];
	$image = $row["picture"];
	$bio = $row["bio"];
	$link = $row["link"];

	if($type != "signals"){
		$total = (($row["a"] + $row["b"] + $row["c"] + $row["d"]) / 40) * 100;
	}
	else{
		$total = $row["total"];
	}
	

}

?>
<br><Br><br><br>
	<div id="left_side" style="float: left; width: 635px;">
		<div id="info_box" style="width: 360px; background-color: white;  margin-left: 150px; margin-top: 80px; text-align: center; box-shadow: 0px 0px 10px lightgrey">
			<br>
			<img src="<?php echo $image ; ?>" style="width: 250px;">
			<Br><br>
			<hr style="width: 250px; background-color: lightgrey; color: red; border: 0.5px solid lightgrey"><br>
			<span style="font-size: 30px"><?php echo $namee; ?></span>
			<br><br><br>
			<span style="font-size: 14px">Beginner Friendly</span>
			<br><Br>
			<span style="font-size: 20px"><b><?php echo $a; ?></b></span>
			<br><br>
			<hr style="width: 100px; background-color: lightgrey; color: red; border: 0.5px solid lightgrey">

			<br><br>
			<span style="font-size: 14px">Visuals</span>
			<br><Br>
			<span style="font-size: 20px"><b><?php echo $b; ?></b></span>
			<br><br>
			<hr style="width: 100px; background-color: lightgrey; color: red; border: 0.5px solid lightgrey">

			<br><br>
			<span style="font-size: 14px">Ease of Setup</span>
			<br><br>
			<span style="font-size: 20px"><b><?php echo $c; ?></b></span>
			<br><br>
			<hr style="width: 100px; background-color: lightgrey; color: red; border: 0.5px solid lightgrey">

			<br><br>



			
			<span style="font-size: 14px">Costs</span>
			<br><Br>
			<span style="font-size: 20px"><b><?php echo $d; ?></b></span>
			<br><br>
			<hr style="width: 100px; background-color: lightgrey; color: red; border: 0.5px solid lightgrey">


			<br><br>
			<span style="font-size: 14px">Total Score</span>
			<br><Br>
			<div style="width: 130px; background-color: #4CCDF7; height: 70px;border-radius: 12px; line-height: 70px; font-size: 20px; text-align: center; margin: auto; color: white; font-family: 'bold'; box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)"><?php echo $total; ?>%</div>
			<br><br>
		</div>
		<br><br>

			<div style="width: 200px; background-color: #4CCDF7; height: 50px;border-radius: 12px; line-height: 50px; font-size: 14px; text-align: center; margin: auto; color: white; font-family: 'bold'; box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)" onclick="location.href='<?php echo $link ;?>'">OPEN ACCOUNT</div>


	</div>
	<div id="right_side" style="float: left; width: calc(100vw - 750px);">
		<div id="detail_box" style="margin-top: 80px; color: #384859; font-size: 14px;	word-spacing: 2px;
	line-height: 23px;
	color: #384859;
">
		<br>
	
		<?php echo $bio ;?>

	</div>

</body>



<link rel="stylesheet" type="text/css" href="index.css">
<style>



@font-face {
  font-family:"test";
  src: url("Poppins-Light.ttf") format("truetype");
}

@font-face {
  font-family:"bold";
  src: url("Poppins-Bold.ttf") format("truetype");
}

h2{
	font-size: 18px;
	font-family: 'bold';
	color: #384859;
}
.detail_content{
	word-spacing: 2px;
	line-height: 23px;
	color: #384859;

}

body, html{
	padding: 0px;
	margin: 0px;
	background-color: #F2F5FA;
	font-size: 14px;
}

#desktop_navigation_bar{
	background-color: rgb(5,34,42);
	height: 130px;
	width: 100vw;
}

*{
	font-family: "test";
	color: rgb(45,45,47);
}

#info_box{
    position:relative;       
	  /*box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);*/
}
/*#info_box:before, #info_box:after
{
    content:"";
    position:absolute; 
    z-index:-1;
    box-shadow:0 0 20px rgba(0,0,0,0.8);
    top:50%;
    bottom:0;
    left:10px;
    right:10px;
    border-radius:100px / 10px;
}*/
}

</style>
<div style="clear: both"></div>
</div>
<div style="clear: both"></div>
</div>
<div style="clear: both"></div></div>
</body>
<div style="width: 100vw; height: auto; background-color: #05222b;">
	<div style="width: calc(100vw - 300px); margin: auto;padding-top: 10px">

		<div id="desktop_navigation_bar_left" style=" margin-left: 0px; line-height: 40px; height: 40px" >
		<div id="desktop_navigation_bar_logo" >
			<img src="arrows.png" id="desktop_navigation_bar_image" style="margin-top: 5px">
		</div>
		<div id="desktop_navigation_bar_name" style="color: grey; line-height: 40px">
			Trading Compared
		</div>
		</div>
	<div style="float: right">
	<a href='brokers.php'><div class="menu_action" style="line-height: 40px; height: 40px">BROKERS</div></a>
	<a href='education.php'><div class="menu_action" style="line-height: 40px; height: 40px">EDUCATION</div></a>
	<a href='signals.php'><div class="menu_action" style="line-height: 40px; height: 40px">SIGNALS</div></a>
	<a href='about.php'><div class="menu_action" style="line-height: 40px; height: 40px">ABOUT US</div></a>
</div>
	<div style="clear:both"></div>
	<Br>

	<div style="font-size: 11px; color: #839eb6; text-align: center"><br>
		Disclaimer: By trading with securities you are taking a high degree of risk. You can lose all of your invested money. You should start trading only if you are aware of this risk. tradingcompared.co.uk is not providing any investment advice, we only help you find the best broker suitable for your needs. tradingcompared is free for everyone, but earns commission from some of the brokers. We get a commission, with no additional cost for you. Please use our link to open your account and we can further provide broker reviews for free.
		<br><br><Br>
		Copyright 2019 TradingCompared All Rights Reserved	
		<br><br>	

	</div>

	</div>

</div>
</html>