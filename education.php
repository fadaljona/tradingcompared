<?php
session_start();

if(!isset($_SESSION["visit"])){
	$ipaddress = $_SERVER['REMOTE_ADDR'];
	$value = $ipaddress;
	$_SESSION["visit"] = $value;
}

include("connect.php");
$val = $_SESSION["visit"];
$time = time();
if(isset($_SESSION["user_id"])){
	$user = $_SESSION["user_id"];
}
else{
	$user = 0;
}

mysqli_query($conn, "INSERT INTO `visits` (`user_id`, `session_key`, `page`, `time`) VALUES ('$user', '$val', 'Education', '$time')");
?>
<!DOCTYPE html>
<html>
<head><!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-135509510-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-135509510-1');
</script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5c9754c0c37db86fcfcf8c16/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
	<title>TradingCompared | Optimising every beginner trader for the market</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="move_to_mobile.js"></script>
<meta name="Description" content="Welcome to TradingCompared. Compare the different brokers, educators and signals available. Trading212, Plus500 and many more.">
<meta name="Keywords" content="compare trader, compare brokers, compare broker, broker, trading, compare trading, optimise markets, trading compared">
	<title></title>
</head>
<body style="background-color: #F2F5FA">

	<div id="desktop_navigation_bar" style="background-color: #05222b;">

		<div id="desktop_navigation_bar_left" >
		<div id="desktop_navigation_bar_logo">
			<img src="arrows.png" id="desktop_navigation_bar_image">
		</div>
		<div id="desktop_navigation_bar_name" onclick='location.href="index.php"'>
			Trading Compared
		</div>
		</div>

	<a href='brokers.php'><div class="menu_action">BROKERS</div></a>
	<a href='education.php'><div class="menu_action">EDUCATION</div></a>
	<a href='signals.php'><div class="menu_action">SIGNALS</div></a>
	<a href='about.php'><div class="menu_action" style='width: 200px'>REGISTER INTEREST</div></a>

	<?php

	if(isset($_SESSION["user_id"])){
		include("connect.php");
		$user_id = $_SESSION["user_id"];
	 	$sql = "SELECT * FROM `users` WHERE `user_id`='$user_id'";
	 	$query = mysqli_query($conn, $sql);
		while($row = mysqli_fetch_assoc($query)){ $name = $row["first_name"]; }
		?>
		<a href='user.php'><div class="menu_action" style="width: 250px">WELCOME <?php echo strtoupper($name); ?></div></a>
		<?php
	}
	else{
		?>
	<div style="width: 320px;float: right;">
	<a href='login.php'><div class="menu_action">LOGIN</div></a>
	<a href='register.php'><div class="menu_action">
		<div id="register">REGISTER</div>
	</div></a>
	</div>
	</div>

		<?php
	}
	?>
</div>
<!-- End of navigation -->

<br><Br><br><br><br><br><br>
<h1 style="text-align: center; font-family: 'bold'">Compare Education</h1>
<br>



			<?php

include("connect.php");
$sql = "SELECT * FROM `education`";
$user = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($user)){

	$total = (($row["a"] + $row["b"] + $row["c"] + $row["d"]) / 40) * 100;

	?>

	<div class="result_card">
		
		<div class="result_card_image" style="float: left; width: 150px">
			<img src="<?php echo $row['picture'];?>" style="width: 130px; margin: 10px; max-height: 70px">
			<!-- <div style="color: black; text-align: center"><b>Trading 212</b></div><br> --><br>
			<div style="width: 130px; background-color: #4bcdf8; height: 40px; margin-top: 10px; margin-left: 10px; border-radius: 6px;
			;color: white; line-height: 40px; text-align: center" onclick="location.href='<?php echo $row["link"]; ?>'"><b>Open Account</b></div>
		</div>
		<div class="result_card_description" style="float: left; width: calc(100% - 775px); height: 100%; margin: 10px; font-size:12px">
			<br><?php echo substr($row['bio'], 16, 120) . "..."; ?><br><a style='text-decoration: none' href='details.php?type=education&id=<?php echo $row['broker_id'] ;?>'><div style="margin-top: 8px;color:#4bcdf8"><b>Read More</b></div></a>
		</div>
		<div class="result_card_score"><br>	Price to<Br>Value<br><span style="font-family: 'bold'; line-height: 5px"><div style="width: 100%; height: 10px"></div><?php echo $row["a"];?></span></div>

		<div class="result_card_score"><br>Amount of<br>content<br><span style="font-family: 'bold'; line-height: : 5px"><div style="width: 100%; height: 10px"></div><?php echo $row["b"];?></span></div>

		<div class="result_card_score"><br>Beginner<br>focused<br><span style="font-family: 'bold'; line-height: : 5px"><div style="width: 100%; height: 10px"></div><?php echo $row["c"];?></span></div>

		<div class="result_card_score"><br>Range of<br>content<br><span style="font-family: 'bold'; line-height: : 5px"><div style="width: 100%; height: 10px"></div><?php echo $row["d"];?></span></div>
		<div class="result_card_total" style="border-left: 1px solid #e3e2e2; width: 120px; float: left; text-align: center; height: calc(100% - 0px);">
			<div style="height: calc(60% - 10px); color: black; margin-top: 10px"><b><br>Total<br>Score</b></div>
			<div style="background-color: #4bcdf8; width: 100%; height: 40%; border-radius: 10px 10px 0px 0px; font-family: 'bold'; line-height: 65px; font-family: 20px; color: white"><?php echo $total; ?>%</div>

		</div>
	</div>
	<Br><br>

<?php

}

?>
<div style="width: 100vw; height: auto; background-color: #05222b;">
	<div style="width: calc(100vw - 300px); margin: auto;padding-top: 10px">

		<div id="desktop_navigation_bar_left" style=" margin-left: 0px; line-height: 40px; height: 40px" >
		<div id="desktop_navigation_bar_logo" >
			<img src="arrows.png" id="desktop_navigation_bar_image" style="margin-top: 5px">
		</div>
		<div id="desktop_navigation_bar_name" style="color: grey; line-height: 40px">
			Trading Compared
		</div>
		</div>
	<div style="float: right">
	<a href='brokers.php'><div class="menu_action" style="line-height: 40px; height: 40px">BROKERS</div></a>
	<a href='education.php'><div class="menu_action" style="line-height: 40px; height: 40px">EDUCATION</div></a>
	<a href='signals.php'><div class="menu_action" style="line-height: 40px; height: 40px">SIGNALS</div></a>
	<a href='about.php'><div class="menu_action" style="line-height: 40px; height: 40px">ABOUT US</div></a>
</div>
	<div style="clear:both"></div>
	<Br>

	<div style="font-size: 11px; color: #839eb6; text-align: center"><br>
		Disclaimer: By trading with securities you are taking a high degree of risk. You can lose all of your invested money. You should start trading only if you are aware of this risk. tradingcompared.co.uk is not providing any investment advice, we only help you find the best broker suitable for your needs. tradingcompared is free for everyone, but earns commission from some of the brokers. We get a commission, with no additional cost for you. Please use our link to open your account and we can further provide broker reviews for free.
		<br><br><Br>
		Copyright 2019 TradingCompared All Rights Reserved	
		<br><br>	

	</div>

	</div>

</div>
<link rel="stylesheet" type="text/css" href="index.css">