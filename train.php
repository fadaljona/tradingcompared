<?php

	$location_a = $_POST["place_a"];
	$location_b = $_POST["place_b"];

	$leaving_time = date("H:i");

	$a = file_get_contents("https://traintimes.org.uk/" . $location_a ."/" . $location_b . "/".$leaving_time."/today");
	
	$a = nl2br($a);
	


	$a = explode('result0', $a);
	$b = explode("tooltip", $a[1]);	
	$b = explode(">", $b[1]);
	$price = explode("</span", $b[1])[0];
	// $location = explode("", $b[2]);
	$type = explode("<",$b[2])[0];

	// echo $location;
	// echo $location[0];
	// print_r($b);

	// print_r($b);

	// print_r($a);
	$train = $a[1];

	$times = explode("<strong>",$a[1]);
	$times = explode("</strong>", $times[1]);
	$start = explode(" ", $times[0])[0];
	$end = explode(" ", $times[0])[2];
	

	$price = $price;
	$type = htmlspecialchars($type);
	$start = htmlspecialchars($start);
	$end = htmlspecialchars($end);

	$date_one = date("D d M Y H:i");
	$date_two = date("D d M Y");

	// echo $price;
	// echo $type;
	// echo $start;
	// echo $end;
	// print_r($times);



?>

<html>

	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	</head>

	<body style="font-family: Arial; padding: 0px; margin: 0px; background-color: #F5F5F5">


		<div style="width: 100%; background-color: #210B16;line-height: 60px; color: white">
			<div style="float: left; margin-left: 10px;">Cancel</div>
			<div style="float: left; position: absolute;left: 35%">Mobile Tickets
			</div>
			<div style="clear: both"></div>
		</div>

		<div style="width: 80%; margin: auto; margin-top: 30px; line-height: 30px; font-size: 18px; color: #444444">
			<div>
				<div style="float: left; width: 60px"><b><?php echo $start; ?></b></div>
				<div style="float: left"><?php echo $location_a; ?></div>
			</div><br>
			<div>
				<div style="float: left; width: 60px"><b><?php echo $end; ?></b></div>
				<div style="float: left"><?php echo $location_b; ?></div>
			</div>
		</div>
		<br><Br>

		<div style="width: 100%; line-height: 70px; text-align: center">
			<div style="float: left; width: 50%" id="barcode_toggle">Barcode</div>
			<div style="float: left; width: 50%; background-color: white" id="ticket_toggle">Ticket</div>
			<div style="clear: both"></div>
		</div>

		<div id="ticket_details" style="background-color: white">

			<div id="ticket" style="padding-top: 10px;padding-bottom: 10px;">
			<div id="top">Activated: <span id="top_label"><?php echo $date_one ;?></span></div>
			<div class="row">
				<div style="width: calc(100%); margin: auto; height: 20px">
					
					<div id="a" style="position: relative;top:0px;float: left; width: calc(33% - 2px); border: 1px solid black; height: 14px; background-color: #CC45CA"></div>
					<div id="b" style="float: left; width: calc(33% - 2px); border: 1px solid black; height: 14px; background-color: #2562AE">
						
					</div>
					<div id="c" style="float: left; width: calc(33% - 2px); border: 1px solid black; height: 14px; background-color: #94894B"></div>

					<div style="width: 100px;position: relative;top: -17px;" id="moving_time">hello</div>
				</div>
			</div>

		<script>

		update_time();


		function update_time(){
			console.log("update time")
			var today = new Date();
			seconds = today.getSeconds();
			minutes = today.getMinutes();
			hours = today.getHours();

			if(today.getSeconds() < 10){
				seconds = "0" +seconds;
			}
			if(today.getMinutes() < 10){
				minutes = "0" +minutes;
			}
			if(today.getHours() < 10){
				hours = "0" + hours;
			}

			document.getElementById("moving_time").innerHTML = hours + ":" + minutes + ":" + seconds;;

			
			setTimeout(update_time, 1000)
		}
	</script>

<style>
	@keyframes animation_fade {
    
   		0% {
        	opacity: 1;
    	}
    	50% {
        	opacity: 0.5;
    	}
    	100%{
    		opacity: 1
    	}
    }
	@keyframes move_time {
    	0%{
    		left: 0%;
    	}
    	25% {
        	
    	}
    	50%{
    		left: +70%;
    	}
    	75%{

    	}
    	100%{
    		left: 0%;
    	}
    }

    #moving_time{
    	animation: move_time 6s infinite;
    }

	#a{
		animation: animation_fade 1.5s infinite;
	}
	#b{
		animation: animation_fade 1.5s infinite;
	}
	#c{
		animation: animation_fade 1.5s infinite;
	}
</style>


			<div class="o_row"><?php echo $type; ?></div>
			<div class="row">SGL(Single)</div>
			<div class="o_row">Adult Standard</div>
			<div class="row"><?php echo $location_a; ?></div>
			<div class="row"><?php echo $location_b; ?></div>
			<div class="o_row">Travel on <?php echo $date_two ;?></div>
			<div class="row">Price: <?php echo $price ;?></div>
			
			<div id="bottom">a</div>
			</div>	
		</div>
		<div id="barcode_details" style="background-color: white; display: none">

			<div id="ticket" style="padding-top: 10px;padding-bottom: 10px;">
			<div id="top">Activated: <span id="top_label"><?php echo $date_one ;?></span></div>
			<div style="width: 100%; background-color: white; text-align: center">
				<img src="https://i.stack.imgur.com/Wfe9Y.png" style="width: 50%">
			</div>
			
			<div id="bottom">a</div>
			</div>	
		</div>
		<div style="text-align: center; line-height: 80px;color: purple">Tickets Active</div>
		<br>

		<table style="font-size: 13px">

			<tr>
				<th>Ticket Number:</th>
				<td>TTDKYNEDY6C</td>
			</tr>
			<tr>
				<th>Passenger Name:</th>
				<td>Ollie</td>
			</tr>
			<tr>
				<th>Purchased on:</th>
				<td><?php echo $date_one ;?></td>
			</tr>
			<tr>
				<th>Activated on:</th>
				<td><?php echo $date_one ;?></td>
			</tr>
			<tr>
				<th>Phone Time:</th>
				<td><?php echo $date_one ;?></td>
			</tr>

		</table>

	</body>


	<style>

	th{
		color: darkgrey;
		font-weight: normal;
		width: 130px;
		text-align: left;
		padding-left: 10px;
	}

	#ticket{
		width: calc(100% - 20px);
		margin: auto;
		font-size: 14px;
		font-weight: bold;

	}

	#top{
		background-color: #FF8300;
		color: #1c1c1c;
		padding: 6px;
		text-align: center;
		border-radius: 10px 10px 0px 0px;
	}
	.row{
		background-color: #FFFDDB;	
		padding-top: 5px;
		padding-bottom: 5px;
		padding-left: 10px;
	}
	.o_row{
		background-color: #EDE9C6;
		padding-top: 5px;
		padding-bottom: 5px;
		padding-left: 10px;
	}
	#bottom{
		background-color: #FF8300;
		color: #FF8300;
		padding: 6px;
		text-align: center;
		border-radius: 0px 0px 10px 10px;
	}

	</style>

</html>


<script>


$("#ticket_toggle").click(function(){
	$("#ticket_details").show();
	$("#barcode_details").hide();
	$("#barcode_toggle").css({
		"background-color" : "f5f5f5",
	})
	$("#ticket_toggle").css({
		"background-color" : "white",
	})
})
$("#barcode_toggle").click(function(){
	$("#barcode_details").show();
	$("#ticket_details").hide();
	$("#barcode_toggle").css({
		"background-color" : "white",
	})
	$("#ticket_toggle").css({
		"background-color" : "f5f5f5",
	})
})


	</script>